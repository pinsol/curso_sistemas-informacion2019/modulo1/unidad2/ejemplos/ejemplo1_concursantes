﻿/**
                                                                         Propuestas nuestras (KVN & Pablo)
  
    1. WHERE
    2. HAVING
    3. WHERE + HAVING
    4. TOTALES
    5. (3. + 4.)
**/

USE concursos;

SELECT * FROM concursantes c;

-- propuestas nuestras

/* 1. Listar los nombres de los concursantes cuya poblacion sea de Santander y cuya altura sea mayor de 170 */
SELECT c.nombre FROM concursantes c WHERE c.poblacion = 'santander' AND c.altura > 170;

/* 2. Indicar que poblacion tiene más de 5 concursantes  */
SELECT c.poblacion FROM concursantes c GROUP BY c.poblacion HAVING COUNT(*) > 5;

/* 3. Mostrar de cada poblacion cuantos concursantes existen, cuyas edades sean mayores de 35 */
SELECT
  c.poblacion,
  COUNT(*) nConcursantes
FROM
  concursantes c
WHERE
  DATEDIFF(NOW(), c.fechaNacimiento) / 365 > 35
    GROUP BY c.poblacion
;

/* 4. Cuál es la altura del concursante más alto*/
SELECT
  MAX(c.altura)
FROM
  concursantes c
;

/* 5. Mostrar el nombre de los concursantes cuya altura supera la altura media de santander */
SELECT
  c.nombre
FROM
  concursantes c
WHERE
  c.poblacion = 'santander'
    GROUP BY c.altura
      HAVING c.altura > AVG(c.altura)
;