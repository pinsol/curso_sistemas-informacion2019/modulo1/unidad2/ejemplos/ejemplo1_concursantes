﻿/**
                                                                         Propuestas de los compañeros
  
    1. WHERE
    2. HAVING
    3. WHERE + HAVING
    4. TOTALES
    5. (3. + 4.)
**/

USE concursos;

SELECT * FROM concursantes c;


/* 1. Listar los concursantes nacidos en 1981 */
SELECT
  nombre
FROM concursantes c
WHERE YEAR(c.fechaNacimiento) = 1981
;

/* 2. Listar las provincias que tengan mas de un concursante */
SELECT
  c.provincia
FROM
  concursantes c
    GROUP BY c.provincia
    HAVING COUNT(*) > 1
;

/* 3. Listar el nombre de los concursantes cuya provincia tenga mas de un concursante y hayan nacido entre 1978 y 1987 */
SELECT
  c.nombre
FROM
  concursantes c
WHERE
  YEAR(c.fechaNacimiento) BETWEEN 1978 AND 1987
   GROUP BY c.provincia
    HAVING COUNT(*) > 1
;

-- con subconsulta
SELECT DISTINCT nombre FROM 
(SELECT c.provincia FROM concursantes c GROUP BY c.provincia HAVING COUNT(*) > 1) c1
JOIN concursantes c USING (provincia) WHERE YEAR(c.fechaNacimiento) BETWEEN 1978 AND 1987;

/* 4. Altura media de los concursantes de Santander */
SELECT
  AVG(c.altura) AlturaMediaSantander
FROM
  concursantes c
WHERE
  c.poblacion = 'santander'
;

/* 5. Altura media de los concursantes de Santander que hayan nacido entre 1978 y 1987 cuya provincia tenga mas de un concursante */
SELECT
  AVG(c.altura)
FROM
  concursantes c
WHERE
  YEAR(c.fechaNacimiento) BETWEEN 1978 AND 1987
AND c.poblacion = 'Santander'
  GROUP BY c.provincia
    HAVING COUNT(*) > 1
;