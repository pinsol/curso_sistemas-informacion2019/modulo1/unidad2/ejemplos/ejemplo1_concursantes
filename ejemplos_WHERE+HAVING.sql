﻿/**
  Ejemplos 1
  Consultas de selección
  Son un conjunto de consultas con totales y selección
  Vamos a trabajar con WHERE, HAVING y GROUP BY
**/

USE concursos;

/* 1. Nombre de los concursantes de Burgos */

-- opcion 1
SELECT
  DISTINCT c.nombre   -- DISTINCT para quitar repetidos
FROM
  concursantes c
WHERE
  c.provincia LIKE 'Burgos' -- el LIKE mejor para comodines '%urgos'
;

-- opcion 2
SELECT
  DISTINCT c.nombre
FROM
  concursantes c
WHERE
  c.provincia = 'Burgos' -- el '=' es más rápido que el LIKE
;

-- opcion mas optimizada LOWER() o UPPER()
SELECT
  DISTINCT c.nombre
FROM
  concursantes c
WHERE
  LOWER(c.provincia) = LOWER('Burgos') -- cambia todos los registros a minúsculas y lo compara con la palabra en minúsculas
;

/* 2. Indicar cuántos concursante hay de Burgos */
SELECT
  COUNT(*) nConcursantesBurgos
FROM
  concursantes c
WHERE
  c.provincia = 'Burgos'
;

/* 3. Indicar cuántos concursantes hay de cada población */
SELECT
  c.poblacion,
  COUNT(*) nConcursantes
FROM
  concursantes c
    GROUP BY c.poblacion
;

/* 4. Indicar las poblaciones que tienen concursantes de menos de 90 kg */
SELECT
  DISTINCT c.poblacion
FROM
  concursantes c
WHERE
  c.peso < 90
;

/* 5. Indicar las poblaciones que tienen más de 1 concursante */
SELECT
  c.poblacion
FROM
  concursantes c
    GROUP BY c.poblacion
    HAVING COUNT(*) > 1
;

/* 6. Indicar las poblaciones que tienen mas de 1 concursante de menos de 90 kg */
SELECT
  c.poblacion
FROM
  concursantes c
WHERE
  c.peso < 90
    GROUP BY c.poblacion
    HAVING COUNT(*) > 1
;